#!/bin/sed -f
#_>>>>>i[[][[[]_011d11_

s:^.*$:_i&_0d0_:

:exec

i\
current state:
h
n
g

#right
s:i>\(.*\)d\(1*\)0:>i\10\2d:
s:d_:d0_:

#left
s:i<\(.*\)0\(1*\)d:<i\1d\20:
s:_d:_0d:

#inc
s:i+\(.*\)d\(1*\)0:+i\1d1\20:

#dec
/i-.*d0/{
	i\
attempt to decrement zero
	q
}
s:i-\(.*\)d1\(1*\)0:-i\1d\20:

#print
/i\./ {
	s:i\.:\.i:
	h
	s:^.*d\(1*\)0.*$:\1:
	s:^.*$:>>>&:
	p
	g
	b exec
}

#read
/i,/ {
	i\
<<<
	N
	s:d1*0\(.*\)\n\(.*\)$:d\20\1:
	s:i,:,i:
	b exec
}

#enter
s:i\[\(.*\)d1:\[i\1d1:

#skip
/i\[.*d0/ {
	s:i\[:\[i:
	h
	s:^.*_\([^_]*i[^_]*\)_.*$:\1:
	
	:strip_s
#	p
	s:i[-+<>,\.]\{1,\}:i:
	t strip_s
	s:i\[\([-[+<>,\.]*\)\]:i\1:
	t strip_s
#	/\[i\]/!i\
#strip s not working
	s:^.*\[i\]::
	G
#	p
	s:\([^\n]*\)\n_\(.*\)\1_0:_\2I\1_0:
	s:i::
	y:I:i:
	b exec
}

#repeat
/i\]/ {
	h
#	p
	s:^.*_\([^_]*i[^_]*\)_.*$:\1:
	:strip_r
#	p
	s:[-+<>,\.]\{1,\}i:i:
	t strip_r
	s:\[\([]+<>,\.-]*\)\]i:\1i:
	t strip_r
	
#	/\[i\]/!i\
#strip r not working
#	i\
#stripped:
#	p
	s:\[i\].*::
	G
#	p
	s:^\([^\n]*\)\n_\1:_\1I:
	s:i::
	y:I:i:
	b exec
}

/i_/{
	s:^_[^_]*_0*::
	s:0*_$::
	i\
end state:
	q
}

b exec
