#!/bin/sed -f
:s
$!{
	N
	b s
}
${
	s:#[^\n]*\n::g
	s:\n\{2,\}:\n:g
	s:exec:e:g
	s:strip_r:sr:g
	s:strip_s:s:g
	s:\\\n\([^\n]*\)\n:BR\1RB:g
	s:\n:;:g
	s:BR:B\nR:g
	s:B\nR\([^\n]*\)RB:\\\n\1\n:g
	s:^$::g
	s:\t::g
	#s:\([^\\]\)\n:\1;:g
	p
}
